﻿using UnityEngine;

namespace IMIRT.SaveSystem
{
    [RequireComponent(typeof(ISaveSystemFactory))]
    public class SaveSystem : MonoBehaviour,ISaveSystem
    {
        private string SaveFileName => "SaveFile";
        
        private IFileFormatHandler m_FileFormatHandler;
        private ISaveFileHandler m_SaveFileHandler;
        private ISaveSystemFactory m_SaveSystemFactory;

        public void Awake()
        {
            m_SaveSystemFactory = GetComponent<ISaveSystemFactory>();
            m_FileFormatHandler = m_SaveSystemFactory.FileFormatHandler();
            m_SaveFileHandler = m_SaveSystemFactory.SaveFileHandler();
        }

        public void Save<T>(T dataObject)
        {
            string data = m_FileFormatHandler.SerializeObject<T>(dataObject); ;
            m_SaveFileHandler.Save(SaveFileName, data);
        }

        public T Load<T>()
        {
            string dataString = m_SaveFileHandler.Load(SaveFileName);
            if (string.IsNullOrEmpty(dataString))
            {
                return default;
            }

            T result = m_FileFormatHandler.DeserializeObject<T>(dataString);
            
            return result != null ? result : default;
        }
    }
}