﻿using UnityEngine;

namespace IMIRT.SaveSystem
{
    public class DefaultSaveSystemFactory : MonoBehaviour,ISaveSystemFactory
    {
        public ISaveFileHandler SaveFileHandler()
        {
#if SAVE_TO_PREFS
                return new PlayerPrefsSaveFileHandler();
#else
                return new FileSystemSaveFileHandler();
#endif
        }

        public IFileFormatHandler FileFormatHandler()
        {
#if USE_BINARY_FORMATTER
                return new BinaryFileFormatHandler();
#else
                // Default behaviour
                return new BinaryFileFormatHandler();
                    
#endif
        }
    }
}
