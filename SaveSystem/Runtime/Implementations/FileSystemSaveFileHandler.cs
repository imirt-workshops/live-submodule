﻿using System.IO;
using UnityEngine;

namespace IMIRT.SaveSystem
{
	public class FileSystemSaveFileHandler : ISaveFileHandler
	{
		public void Delete(string key)
		{
			if (!string.IsNullOrEmpty(key))
			{
				FileInfo fileInfo = new FileInfo(GetFilePath(key));
				if (fileInfo.Exists)
				{
					fileInfo.Delete();
				}
			}
		}

		public bool Exists(string key)
		{
			return File.Exists(GetFilePath(key));
		}

		public string Load(string key)
		{
			string _data = string.Empty;

			if (Exists(key))
			{
				StreamReader streamReader = File.OpenText(GetFilePath(key));
				string _info = streamReader.ReadToEnd();
				streamReader.Close();
				_data = _info;
			}
			return _data;
		}

		public void Save(string key, string data)
		{
			string fullFilename = GetFilePath(key);			
			FileInfo fileInfo = new FileInfo(fullFilename);
			StreamWriter writer = fileInfo.CreateText();
			writer.Write(data);
			writer.Close();
		}

		private string GetFilePath(string key)
		{
			string normalSaveDirectory = Application.persistentDataPath;
			normalSaveDirectory = normalSaveDirectory.Replace('/', Path.DirectorySeparatorChar);
			return normalSaveDirectory + Path.DirectorySeparatorChar.ToString() + key;
		}
	}

}