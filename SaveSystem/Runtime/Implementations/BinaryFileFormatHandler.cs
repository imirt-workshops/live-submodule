﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace IMIRT.SaveSystem
{
	public class BinaryFileFormatHandler : IFileFormatHandler
	{

		public string SerializeObject<T>(object dataObject)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			MemoryStream memoryStream = new MemoryStream();
			binaryFormatter.Serialize(memoryStream, dataObject);
			return (Convert.ToBase64String(memoryStream.GetBuffer()));
		}


		public T DeserializeObject<T>(string dataString)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			MemoryStream memoryStream = new MemoryStream(Convert.FromBase64String(dataString));
			return (T) binaryFormatter.Deserialize(memoryStream);
		}
	}
}
